import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IndexComponent} from './pages/index/index.component';
import {BioComponent} from './pages/bio/bio.component';
import {ConstructionComponent} from './pages/construction/construction.component';
import {ServicesComponent} from './pages/services/services.component';
import {ContactComponent} from './pages/contact/contact.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'biografia', component: BioComponent },
  { path: 'servicios', component: ServicesComponent },
  { path: 'obra', component: ConstructionComponent },
  { path: 'contacto', component: ContactComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
