import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  @Input() background: string | undefined;

  open = false;

  constructor() { }

  ngOnInit(): void {
  }


  openMenu(){
    if (!this.open){
      this.open = true;
    } else {
      this.open = false;
    }
  }

}
